#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 22:09:01 2018

@author: minhnd
"""

#%%
import numpy as np
from os.path import join, exists
from os import makedirs
import cv2
#%%
def extract_images(dataset, out_dir, type_):
    assert type_ == 'train' or type_ == 'test'
    type_dir = join(out_dir, 'dataset', type_)
    if not exists(type_dir):
        makedirs(type_dir)
    num_files = dataset.shape[0]
    for i in range(num_files):
        if dataset[i, 1] == 1:
            yes_dir = join(type_dir, 'yes')
            if not exists(yes_dir):
                makedirs(yes_dir)
            # print(type(dataset[i, 0][:,:,0].astype("uint8")))
            cv2.imwrite(join(yes_dir, type_ + str(i) + '.png'), dataset[i, 0][:,:,0].astype("uint8"))
        else:
            no_dir = join(type_dir, 'no')
            if not exists(no_dir):
                makedirs(no_dir)
            # print(type(dataset[i, 0][:,:,0].astype("uint8")))
            cv2.imwrite(join(no_dir, type_ + str(i) + '.png'), dataset[i, 0][:,:,0].astype("uint8"))

#%%
if __name__ == "__main__":
    DATASET_FOLDER = join('/', 'home', 'minhnd', 'Desktop', 'SmileDataset')
    smile_train = np.load(join(DATASET_FOLDER, 'train.npy'))
    smile_test = np.load(join(DATASET_FOLDER, 'test.npy'))

    extract_images(smile_train, DATASET_FOLDER, 'train')
    extract_images(smile_test, DATASET_FOLDER, 'test')