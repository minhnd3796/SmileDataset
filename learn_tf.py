import tensorflow as tf

def defineVar():
    a = tf.get_variable(name='a', shape=(), dtype=tf.float32)
    return a

with tf.variable_scope('var1'):
    a1 = defineVar()
with tf.variable_scope('var2'):
    a2 = defineVar()

define1 = tf.assign(a1, 1, name='assign1')
define2 = tf.assign(a2, 2, name='assign2')

sess = tf.InteractiveSession()
writer = tf.summary.FileWriter('./summary2/')
writer.add_graph(sess.graph)

sess.run(tf.global_variables_initializer())