import tensorflow as tf

a = tf.get_variable('a', shape=(), dtype=tf.float32)
b = tf.get_variable('b', shape=(), dtype=tf.float32)

writer = tf.summary.FileWriter('./summary3/')
tf.summary.scalar('var_a', a)
tf.summary.scalar('var_b', b)
merge_summary = tf.summary.merge_all()

sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

for i in range(100):
    sess.run(tf.assign(a, i))
    sess.run(tf.assign(b, 100 - i))
    s = sess.run(merge_summary)
    writer.add_summary(s, i)