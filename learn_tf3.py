import tensorflow as tf
#%%
for turn in range(10):
    a = tf.get_variable('a', shape=(), dtype=tf.float32)
    b = tf.get_variable('b', shape=(), dtype=tf.float32)
    
    writer = tf.summary.FileWriter('./summary3/' + str(turn))
    tf.summary.scalar('var_a', a)
    tf.summary.scalar('var_b', b)
    merge_summary = tf.summary.merge_all()
    
    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())
    
    for i in range(100):
        sess.run(tf.assign(a, i + 10 * turn))
        sess.run(tf.assign(b, 100 - i + 10 * turn))
        s = sess.run(merge_summary)
        writer.add_summary(s, i)
        
    sess.close()
    tf.reset_default_graph()
#%%