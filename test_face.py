#%%
import tensorflow as tf
import cv2
import numpy as np
import sys
#%%
with tf.Session() as sess:
    new_saver = tf.train.import_meta_graph('smily_face_model/smily_face_recogniser.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('smily_face_model'))
    # print(sess.run('a:0'))
    # print(sess.run('b:0'))
    # x = tf.placeholder(dtype=tf.float64, name='x')
    # x = tf.placeholder(dtype=tf.float32, shape=[None, 48, 48, 1], name='input')
    
    graph = tf.get_default_graph()
    # _in = graph.get_tensor_by_name("input:0")
    # out = graph.get_tensor_by_name("linear/linear:0")
    
    feed_dict = {graph.get_tensor_by_name("input:0"): cv2.imread(sys.argv[1], 0)[np.newaxis, :, :, np.newaxis].astype(np.float32)}
    print(sess.run(tf.nn.softmax(graph.get_tensor_by_name("linear/linear:0"), axis=1), feed_dict))

    # kernel = graph.get_tensor_by_name("Block1/kernel:0")
    # print(sess.run(kernel))
#%%