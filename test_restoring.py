import tensorflow as tf

with tf.Session() as sess:
    new_saver = tf.train.import_meta_graph('saved_model/my_test_model.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('saved_model'))
    # print(sess.run('a:0'))
    # print(sess.run('b:0'))
    x = tf.placeholder(dtype=tf.float32, name='x')
    
    graph = tf.get_default_graph()
    # a = graph.get_tensor_by_name("mul_phase/weight:0")
    # b = graph.get_tensor_by_name("add_phase/bias:0")
    # out = tf.add(tf.multiply(a, x), b)
    out = graph.get_tensor_by_name("add_phase/sum:0")
    print(sess.run(out, feed_dict={graph.get_tensor_by_name("input:0"):4.0}))
    feed_dict = {x: 4.0}

    # y = x * a + b
    # print(sess.run(out, feed_dict))