#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 10:12:36 2018

@author: minhnd
"""

import tensorflow as tf

def _input():
    x = tf.placeholder(dtype=tf.float32, name='input')
    return x

def _mul(x, in_val):
    w = tf.get_variable(name='weight', dtype=tf.float32,
        initializer=tf.constant_initializer(in_val), shape=())
    output = tf.multiply(x, w, name='prod')
    return output

def _add(x, in_val):
    b = tf.get_variable(name='bias', dtype=tf.float32,
        initializer=tf.constant_initializer(in_val), shape=())
    output = tf.add(x, b, name='sum')
    return output

def inference(x):
    with tf.variable_scope('mul_phase'):
        output = _mul(x, 5.0)
    
    with tf.variable_scope('add_phase'):
        output = _add(output, 3.0)
    
    return output

sess = tf.Session()
x = _input()

out = inference(x)
sess.run(tf.global_variables_initializer())

print(sess.run(out, feed_dict={x:4.0}))
saver = tf.train.Saver()
saver.save(sess, 'saved_model/my_test_model')